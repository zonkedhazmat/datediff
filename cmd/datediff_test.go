package cmd

import "testing"

func TestDateDiffs(t *testing.T) {
	dates := []struct {
		startDate Date
		endDate   Date
		days      int
	}{
		// Simple initial multi day tests
		{Date{1, 1, 2000}, Date{1, 1, 2000}, 0},
		{Date{1, 1, 2000}, Date{2, 1, 2000}, 0},
		{Date{1, 1, 2000}, Date{3, 1, 2000}, 1},
		{Date{1, 1, 2000}, Date{4, 1, 2000}, 2},
		// Across year case
		{Date{31, 12, 1999}, Date{1, 1, 2000}, 0},
		{Date{31, 12, 1999}, Date{2, 1, 2000}, 1},
		{Date{30, 12, 1999}, Date{1, 1, 2000}, 1},
		{Date{29, 12, 1999}, Date{4, 1, 2000}, 5},
		// across multi year
		{Date{31, 12, 1999}, Date{1, 1, 2020}, 7305},
		{Date{29, 12, 1999}, Date{4, 1, 2020}, 7310},
		// multi year multi month
		{Date{29, 11, 1999}, Date{4, 4, 2020}, 7431},
		// reversed dates
		{Date{30, 12, 1999}, Date{4, 2, 2000}, 35},
		{Date{11, 10, 1991}, Date{9, 10, 1991}, 1},
		// Across month
		{Date{31, 1, 2000}, Date{1, 2, 2000}, 0},
		{Date{29, 1, 2000}, Date{4, 2, 2000}, 5},
		// Across multi months same year
		{Date{31, 1, 2000}, Date{1, 4, 2000}, 60},
		{Date{29, 1, 2000}, Date{4, 4, 2000}, 65},
		// given test cases
		{Date{2, 6, 1983}, Date{22, 6, 1983}, 19},
		{Date{4, 7, 1984}, Date{25, 12, 1984}, 173},
		{Date{3, 1, 1989}, Date{3, 8, 1983}, 1979},
	}
	for _, date := range dates {
		days := DateDiff(date.startDate, date.endDate)
		if days != date.days {
			t.Errorf("The difference between dates %d - %d was incorrect got %d instead of %d",
				date.startDate, date.endDate, days, date.days)
		}
	}
}
