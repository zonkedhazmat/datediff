package cmd

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

type Date struct {
	day   int
	month int
	year  int
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "datediff",
	Short: "",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		var err error
		if len(args) < 3 || args[1] != "-" {
			err = errors.New("input format must be of dd/mm/yyyy - dd/mm/yyyy")
			return err
		}
		startDate, err := parseDate(args[0])
		if err != nil {
			return err
		}
		endDate, err := parseDate(args[2])
		if err != nil {
			return err
		}
		days := DateDiff(startDate, endDate)
		//daysCorrect := daysCorrect(startDate, endDate)
		//fmt.Println("Correct days:", daysCorrect)
		fmt.Println("Days in period:", days)
		return nil
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.
	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.datediff.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".datediff" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".datediff")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}

func isLeapYear(year int) bool {
	if year%400 == 0 {
		return true
	} else if year%100 == 0 {
		return false
	} else if year%4 == 0 {
		return true
	}
	return false
}

func daysInMonth(month int, isLeap bool) int {
	if month == 2 {
		if isLeap {
			return 29
		} else {
			return 28
		}
	} else if month == 4 || month == 6 || month == 9 || month == 11 {
		return 30
	}
	return 31
}

func remainingDaysInMonth(day int, daysInMonth int) int {
	return daysInMonth - day
}

func parseDate(inputDate string) (Date, error) {
	var date Date
	var err error

	if len(inputDate) != 10 || inputDate[2] != '/' || inputDate[5] != '/' {
		return date, errors.New("date needs to be of format dd/mm/yyyy")
	}

	dateArr := strings.Split(inputDate, "/")
	date.day, err = strconv.Atoi(dateArr[0])
	if err != nil {
		return date, err
	}

	date.month, err = strconv.Atoi(dateArr[1])
	if err != nil {
		return date, err
	}
	if date.month < 1 || date.month > 12 {
		return date, errors.New("month must be a number 01 - 12")
	}

	date.year, err = strconv.Atoi(dateArr[2])
	if err != nil {
		return date, err
	}
	if numDays := daysInMonth(date.month, isLeapYear(date.year)); date.day > numDays {
		errStr := "the month supplied only has " + strconv.Itoa(numDays) + " not the supplied " + strconv.Itoa(date.day)
		return date, errors.New(errStr)
	}

	return date, nil
}

func DateDiff(startDate Date, endDate Date) int {
	var days int
	if endDate.year < startDate.year || endDate.year == startDate.year && endDate.month < startDate.month ||
		endDate.year == startDate.year && endDate.month == startDate.month && endDate.day < startDate.day {
		startDate, endDate = endDate, startDate
	}
	if endDate.year != startDate.year {
		days += remainingDaysInYear(startDate)
		for i := startDate.year + 1; i < endDate.year; i++ {
			days += daysInFullYear(i)
		}
		days += daysInLastYear(endDate)
	} else if endDate.month != startDate.month {
		days = daysInPartialYear(startDate, endDate)
	} else {
		days = endDate.day - startDate.day
		// Subtract one as we are only counting days in between the dates
		if days != 0 {
			days -= 1
		}
	}
	return days
}

func remainingDaysInYear(date Date) int {
	var days int
	isLeap := isLeapYear(date.year)
	days += remainingDaysInMonth(date.day, daysInMonth(date.month, isLeap))
	for i := date.month + 1; i <= 12; i++ {
		days += daysInMonth(i, isLeap)
	}
	return days
}

func daysInLastYear(date Date) int {
	var days int
	isLeap := isLeapYear(date.year)
	for i := 1; i < date.month; i++ {
		days += daysInMonth(i, isLeap)
	}
	// Subtract one as we are only counting days in between the dates
	days += date.day - 1
	return days
}

func daysInFullYear(year int) int {
	if isLeapYear(year) {
		return 366
	}
	return 365
}

func daysInPartialYear(startDate Date, endDate Date) int {
	var days int
	isLeap := isLeapYear(startDate.year)
	days += remainingDaysInMonth(startDate.day, daysInMonth(startDate.month, isLeap))
	for i := startDate.month + 1; i < endDate.month; i++ {
		days += daysInMonth(i, isLeap)
	}
	// Subtract one as we are only counting days in between the dates
	days += endDate.day - 1
	return days
}

// cli testing
func date(year, month, day int) time.Time {
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}

func daysCorrect(startDate Date, endDate Date) float64 {
	if endDate.year < startDate.year || endDate.year == startDate.year && endDate.month < startDate.month {
		startDate, endDate = endDate, startDate
	}
	d1 := date(startDate.year, startDate.month, startDate.day)
	d2 := date(endDate.year, endDate.month, endDate.day)
	daysCorrect := d2.Sub(d1).Hours() / 24
	if daysCorrect != 0 {
		daysCorrect -= 1
	}
	return daysCorrect
}
